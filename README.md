## zer0.k's NohBoard Config
Modified version of Zach47's NohBoard config, compatible with NohBoard v1.2.2

---

## [How it looks](https://i.imgur.com/6omMYPL.png)

## Get NohBoard [here](https://github.com/ThoNohT/NohBoard)

## Keybinds:

- Checkpoint: 1
- Gocheck: 2
- Crouch: Left Ctrl
- Start/Reset: Mouse4 or 4

## Installation guide:

1. Install NohBoard (if you haven't already)
2. Download and extract `keyboards.rar` into NohBoard's install directory
3. Open NohBoard, right click and choose `Load Keyboard`
4. Choose `zer0.k's config` category and use the default keyboard style
5. Enjoy :)